﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using EInvoice.ServiceReference1;
using EInvoice.ServiceReference2;


namespace EInvoice
{
    public class MyEInvoice
    {
        private EInvoic einvoice;
        private Certificate cert;

        protected T Deserialize<T>(string xmlString)
        {
            T result;
            XmlSerializer ser = new XmlSerializer(typeof(T));
            using (StringReader sr = new StringReader(xmlString))
            {
                result = (T)ser.Deserialize(sr);
            }
            return result;
        }

        protected string Serialize(object obj)
        {
            XmlSerializer ser = new XmlSerializer(obj.GetType());
            using (StringWriter sw = new StringWriter())
            {
                ser.Serialize(sw, obj);
                return sw.ToString();
            }
        }


        public void DoFisc()
        {
            try
            {
                string  companyName, currentExitID,fr;
                dynamic companyData, invoiceItems, itemsWithDisc = null;
                string iic, fic, issuedDT, payType, TCRCode, BUCode, OperatorCode = "";
                dynamic invDetails = null;
                //IEnumerable<InvItemsWithDisc> invDiscs = null;
                //IEnumerable<InvHeader> invHeader = null;
                //IEnumerable<InvVAT> invVAT = null;
                //IEnumerable<InvVATCount> invVATCount = null;
                //items2 = new List<FiscalData>();
                //bool isCorrectiveInv;
                //string docT;

                fr = "";
                string webServiceAddress = AppConfig.ServiceURI;
                cert = new Certificate(AppConfig.KeyStoreLocation, AppConfig.KeyStorePass);

                using (var db2 = new PetaPoco.Database("CNFisc"))
                {
                    var reqs = db2.Query<dynamic>(
                        "Select tblFiscalData.ExitID,tblExit.IIC,DocType,Min(cast(cast(Request1 as varbinary(max)) as varchar(max))) as Request1 ,Min(cast(cast(FullRequest as varbinary(max)) as varchar(max))) as FullRequest   From Fiscal.dbo.tblExit inner join Fiscal.dbo.tblFiscalData on tblExit.ExitID = tblFiscalData.ExitID  Where (IsDoneFiscal=0 or IsDoneFiscal IS NULL) AND (DocType<>'2' or DocType Is Null) GROUP by tblFiscalData.ExitID,tblExit.IIC,DocType");
                    foreach (dynamic req in reqs)
                    {
                        iic = (string) req.IIC;
                        //docType = (string) req.DocType;
                        fr = (string) req.Request1; //FullRequest;Request1
                        RegisterInvoiceRequest ri2 = new RegisterInvoiceRequest();
                        ri2 = Deserialize<RegisterInvoiceRequest>(fr);

                        einvoice = new EInvoic(cert,   webServiceAddress);

                    }
                }

                


            }
            catch (Exception ex)
            {

                //    MessageBox.Show("Ndodhi nje problem. Ju lutem njoftoni departamentin IT : " + ex.Message);
                //    items2.Add(new FiscalData { ID2 = "RegisterInvoice", Request1 = System.Text.Encoding.UTF8.GetBytes(AppConfig.Req), Response1 = System.Text.Encoding.UTF8.GetBytes(ex.Message), T1 = DateTime.Now, T2 = DateTime.Now });
                //    SaveInDB(items2, currentExitID, AppConfig.GlobalIIC, "", "", "", "", "");
            }
            finally
            {

            }


        }
    }
}
