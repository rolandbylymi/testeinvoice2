﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Security.Cryptography;
using System.Xml;

namespace EInvoice
{
    public class Certificate
    {

        public const String XML_REQUEST_ID = "Request";
        public const String XML_SIG_METHOD = "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256";
        public const String XML_DIG_METHOD = "http://www.w3.org/2001/04/xmlenc#sha256";
        X509Certificate2 _cert = null;
        RSA privateKey;

        private X509Certificate2 GetCertificateByThumbprint(string fileName, string password)
        {
            X509Certificate2 cert = null;
            //X509Store store = new X509Store(StoreName.Root, StoreLocation.LocalMachine);

            X509Certificate2 keyStore = new X509Certificate2(fileName, password);
            cert = keyStore;

            privateKey = (RSACryptoServiceProvider)cert.PrivateKey;
            return cert;
        }

        public Certificate(string fileName, string password)
        {
            try
            {
                this._cert = GetCertificateByThumbprint(fileName, password);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public class SignedXmlWithId : SignedXml
        {
            public SignedXmlWithId(XmlDocument xml) : base(xml)
            {
            }

            public SignedXmlWithId(XmlElement xmlElement)
                : base(xmlElement)
            {
            }

            public override XmlElement GetIdElement(XmlDocument doc, string id)
            {
                // check to see if it's a standard ID reference
                XmlElement idElem = base.GetIdElement(doc, id);

                if (idElem == null)
                {
                    XmlNamespaceManager nsManager = new XmlNamespaceManager(doc.NameTable);
                    nsManager.AddNamespace("wsu", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");

                    idElem = doc.SelectSingleNode("//*[@wsu:Id=\"" + id + "\"]", nsManager) as XmlElement;
                }

                return idElem;
            }
        }

        public SignedXml GetSignedXmlDoc(string referenceStr, XmlDocument doc)
        {
            doc.PreserveWhitespace = false;

            SignedXml signedXml = new SignedXml(doc);
            //signedXml.SigningKey = (RSACryptoServiceProvider)_cert.PrivateKey;
            var privKey = (RSACryptoServiceProvider)_cert.PrivateKey;
            // Force use of the Enhanced RSA and AES Cryptographic Provider with openssl-generated SHA256 keys
            var enhCsp = new RSACryptoServiceProvider().CspKeyContainerInfo;
            var cspparams = new CspParameters(enhCsp.ProviderType, enhCsp.ProviderName, privKey.CspKeyContainerInfo.KeyContainerName);
            privKey = new RSACryptoServiceProvider(cspparams);
            signedXml.SigningKey = privKey;
            signedXml.SignedInfo.CanonicalizationMethod = SignedXml.XmlDsigExcC14NTransformUrl;
            signedXml.SignedInfo.SignatureMethod = XML_SIG_METHOD;

            Reference reference = new Reference(referenceStr);
            reference.AddTransform(new XmlDsigEnvelopedSignatureTransform(false));
            reference.AddTransform(new XmlDsigExcC14NTransform(false));
            reference.DigestMethod = XML_DIG_METHOD;
            //reference.Uri = "#" + XML_REQUEST_ID;
            signedXml.AddReference(reference);


            KeyInfo keyInfo = new KeyInfo();
            KeyInfoX509Data keyInfoData = new KeyInfoX509Data();
            keyInfoData.AddCertificate(_cert);
            //keyInfoData.AddIssuerSerial(_cert.Issuer, _cert.GetSerialNumberString());
            keyInfo.AddClause(keyInfoData);
            signedXml.KeyInfo = keyInfo;

            signedXml.ComputeSignature();

            return signedXml;
        }



        public static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        public string GetSignatureFromSignAndHashString(string str)
        {
            RSA privateKey = _cert.GetRSAPrivateKey();
            byte[] iicSignature = privateKey.SignData(Encoding.ASCII.GetBytes(str), HashAlgorithmName.SHA256,
                RSASignaturePadding.Pkcs1);
            string iicSignatureString = BitConverter.ToString(iicSignature).Replace("-", string.Empty);

            return iicSignatureString;
        }

        public string GetIICFromSignAndHashString(string str)
        {
            RSA privateKey = _cert.GetRSAPrivateKey();
            byte[] iicSignature = privateKey.SignData(Encoding.ASCII.GetBytes(str), HashAlgorithmName.SHA256,
                RSASignaturePadding.Pkcs1);
            // string iicSignatureString = BitConverter.ToString(iicSignature).Replace("-", string.Empty);

            byte[] iic = ((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(iicSignature);
            string iicString = BitConverter.ToString(iic).Replace("-", string.Empty);

            return iicString;
        }
    }
}
