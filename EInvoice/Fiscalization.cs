﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Configuration;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Windows.Forms;
using System.Xml.Linq;
using Message = System.ServiceModel.Channels.Message;

namespace EInvoice
{
    public class Fiscalization
    {
        protected Certificate cert = null;

        protected string Serialize(object obj)
        {
            XmlSerializer ser = new XmlSerializer(obj.GetType());
            using (StringWriter sw = new StringWriter())
            {
                ser.Serialize(sw, obj);
                return sw.ToString();
            }
        }

        protected T Deserialize<T>(string xmlString)
        {
            T result;
            XmlSerializer ser = new XmlSerializer(typeof(T));
            using (StringReader sr = new StringReader(xmlString))
            {
                result = (T)ser.Deserialize(sr);
            }
            return result;
        }

        protected static string AddNamespace(string str, string rootTag)
        {
            string pattern = @"xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""";
            string replacement = @"xmlns=""https://Einvoice.tatime.gov.al/EinvoiceService/schema""";
            Regex reg = new Regex(pattern);
            str = reg.Replace(str, replacement);

         
            pattern = @"xmlns:xsd=""http://www.w3.org/2001/XMLSchema""";
            replacement = "";
            reg = new Regex(pattern);
            str = reg.Replace(str, replacement);

            //pattern = @"<(/?)((?!" + rootTag + ")[^/<>]+)>"; // (?!RacunType) - negative lookahead
            //replacement = @"<$1tns:$2>";
            //reg = new Regex(pattern);
            //str = reg.Replace(str, replacement);

            return str;
        }



        public string rawRequestStr = "";
        public string requestStr = "";
        public string responseStr = "";

        public Fiscalization(string fileName, string password)
        {
            this.cert = new Certificate(fileName, password);
        }

        public Fiscalization(Certificate certificate)
        {
            if (certificate == null) throw new ArgumentNullException("certificate");
            this.cert = certificate;
        }

        // Used to intercept SOAP message and sign it...just like that.
        // http://msdn.microsoft.com/en-us/library/aa717047.aspx
        public class Inspector : IClientMessageInspector
        {
            Fiscalization that; // used so we can reference outer class members
            internal Inspector(Fiscalization f)
            {
                that = f;
            }

            public object BeforeSendRequest(ref Message request, IClientChannel channel)
            {
                Message oldRequest = request;

                MessageBuffer requestBuf = oldRequest.CreateBufferedCopy(int.MaxValue);
                Message tmpRequest = requestBuf.CreateMessage();
                XmlDocument doc = new XmlDocument();
                using (XmlDictionaryReader xdr = tmpRequest.GetReaderAtBodyContents())
                {
                    doc.Load(xdr);
                    xdr.Close();
                }

                System.Security.Cryptography.Xml.SignedXml signedDoc = that.cert.GetSignedXmlDoc("#Request", doc);
                XmlElement signature = signedDoc.GetXml();

                doc.DocumentElement.AppendChild(doc.ImportNode(signature, true));

                //AppConfig.ReqInnerXML = doc.InnerXml;

                MemoryStream ms = new MemoryStream();
                XmlWriter xw = XmlWriter.Create(ms);
                doc.Save(xw);
                xw.Flush();
                xw.Close();

                ms.Position = 0;

                XmlReader xr = XmlReader.Create(ms);
                request = Message.CreateMessage(oldRequest.Version, null, xr);
                request.Headers.CopyHeaderFrom(oldRequest, 0);
                request.Properties.CopyProperties(oldRequest.Properties);

                requestBuf = request.CreateBufferedCopy(int.MaxValue);
                tmpRequest = requestBuf.CreateMessage();
                that.requestStr = tmpRequest.ToString();

                request = tmpRequest;

                return null;
            }

            public void AfterReceiveReply(ref Message reply, object correlationState)
            {
                that.responseStr = reply.ToString();
                //byte[] responseBytes = System.Text.Encoding.UTF8.GetBytes(that.responseStr);
                return;
            }
        }

        public class EndpointBehavior : IEndpointBehavior
        {
            Fiscalization that;
            internal EndpointBehavior(Fiscalization f)
            {
                that = f;
            }

            public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
            {
                Inspector inspector = new Inspector(that);
                clientRuntime.MessageInspectors.Add(inspector);
            }

            public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
            {
                return;
            }

            public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
            {
                return;
            }

            public void Validate(ServiceEndpoint endpoint)
            {
                return;
            }
        }
    }
}
