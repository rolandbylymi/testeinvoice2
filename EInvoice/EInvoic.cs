﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using EInvoice;
using EInvoice.ServiceReference1;
using EInvoice.ServiceReference2;
using System.Xml;
using System.Xml.Linq;

namespace EInvoice
{
    public class EInvoic : Fiscalization
    {
        private string _xmlString = "";
        private string _webServiceAddress = "";
        //private RegisterInvoiceRequest req = new RegisterInvoiceRequest();
        //private RegisterInvoiceResponse resp = new RegisterInvoiceResponse();
        RegisterEinvoiceRequest req = new RegisterEinvoiceRequest();
        RegisterEinvoiceResponse resp = new RegisterEinvoiceResponse();
        private Certificate _cert;
        public string IIC, FIC, TCRCode, BUCode, OperatorCode, PayType = "";
        public DateTime IssuedDT;
        public bool isCorrectiveInv;
        //private InvHeader firstInvHeader;
        private string CashCode = "";


        public EInvoic(Certificate cert, string webServiceAddress = "")
            : base(cert)
        {
            _webServiceAddress = webServiceAddress;
            _cert = cert;

            req.Id = "Request";
            req.Version = 1;
            
            var header = new RegisterEinvoiceRequestHeaderType()
            {
                UUID = Guid.NewGuid().ToString(),
                SendDateTime = DateTime.ParseExact(DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ"), "yyyy-MM-ddTHH:mm:ssZ", new System.Globalization.CultureInfo("fr-FR")),
            };

            req.Header = header;
            req.EinvoiceEnvelope = new EinvoiceEnvelopeType();
            req.EinvoiceEnvelope.ItemElementName = ItemChoiceType.UblInvoice;

            var fileBytes = File.ReadAllBytes(@"C:\ACC\zzzzz.xml");
            //string encodedFile = Convert.ToBase64String(fileBytes);




            var doc = new XmlDocument();
            var str = System.Text.Encoding.UTF8.GetString(fileBytes);
            doc.LoadXml(str);

            XmlDocument clone = (XmlDocument)doc.CloneNode(true);

            if (doc.FirstChild.NodeType == XmlNodeType.XmlDeclaration)
                doc.RemoveChild(doc.FirstChild);

            XmlNodeList nodes2 = doc.GetElementsByTagName("ext:UBLExtensions");
            nodes2[0].ParentNode.RemoveChild(nodes2[0]);

            System.Security.Cryptography.Xml.SignedXml signedDoc = cert.GetSignedXmlDoc("", doc);
            XmlElement signature = signedDoc.GetXml();
            //doc.DocumentElement.AppendChild(doc.ImportNode(signature, true));


            //XmlNode root = doc.DocumentElement;
            ////Create a new node.
            //XmlElement elem = doc.CreateElement("UBLExtension");
            //elem.SetAttribute("a", "B","C");
            //elem.InnerText = "19.95";
            ////Add the node to the document.
            //root.AppendChild(elem);



            //var doc2 = new XmlDocument();
            //doc2.LoadXml("<ext:UBLExtensions> <ext:UBLExtension xmlns:sac=\"urn: oasis: names: specification: ubl: schema: xsd: SignatureAggregateComponents - 2\" xmlns:sig=\"urn: oasis: names: specification: ubl: schema: xsd: CommonSignatureComponents - 2\"> <ext:ExtensionContent> <sig:UBLDocumentSignatures> <sac:SignatureInformation> </sac:SignatureInformation> </sig:UBLDocumentSignatures> </ext:ExtensionContent> </ext:UBLExtension> </ext:UBLExtensions> ");

            //XmlDocumentFragment xfrag = doc.CreateDocumentFragment();
            //xfrag.InnerXml = "<ext:UBLExtensions> <ext:UBLExtension xmlns:sac=\"urn: oasis: names: specification: ubl: schema: xsd: SignatureAggregateComponents - 2\" xmlns:sig=\"urn: oasis: names: specification: ubl: schema: xsd: CommonSignatureComponents - 2\"> <ext:ExtensionContent> <sig:UBLDocumentSignatures> <sac:SignatureInformation> </sac:SignatureInformation> </sig:UBLDocumentSignatures> </ext:ExtensionContent> </ext:UBLExtension> </ext:UBLExtensions> ";
            //doc.DocumentElement.FirstChild.AppendChild(xfrag);

            //doc.DocumentElement.AppendChild(CreateXMLElement(""));


            //XmlNodeList nodes3 = clone.GetElementsByTagName("ext:UBLExtensions");
            ////nodes3[0].ParentNode.RemoveChild(nodes3[0]);
            //nodes3[0].ParentNode.ReplaceChild(clone.ImportNode(signature, true), nodes3[0]);


            //XmlNodeList nodes3 = clone.GetElementsByTagName("Signature");
            //nodes3[0].ParentNode.ReplaceChild(clone.ImportNode(signature, true), nodes3[0]);

            var extensions = clone.GetElementsByTagName("Signature");
            extensions.Item(0).InnerXml = signature.InnerXml;

            clone.ImportNode(extensions.Item(0), true);
            clone.PreserveWhitespace=true;

            File.WriteAllBytes(@"C:\acc\a.xml", System.Text.Encoding.UTF8.GetBytes(clone.InnerXml));
            //req.EinvoiceEnvelope.Item = System.Text.Encoding.UTF8.GetBytes(clone.InnerXml); //fileBytes
            req.EinvoiceEnvelope.Item = fileBytes;



            //AppConfig.Req = ToString();
            //AppConfig.GlobalIIC = req.Invoice.IIC;
            //AppConfig.GlobalTCRCode = req.Invoice.TCRCode;
            //AppConfig.GlobalBUCode = req.Invoice.BusinUnitCode;
            //AppConfig.GlobalOperatorCode = req.Invoice.OperatorCode;
            rawRequestStr = ToString();
            Send(webServiceAddress);
            //rawRequestStr = ToString();
            //requestStr = base.requestStr;
            //responseStr = base.responseStr;
            //IIC = req.Invoice.IIC;
            //FIC = resp.FIC;
            //TCRCode = req.Invoice.TCRCode;
            //BUCode = req.Invoice.BusinUnitCode;
            //OperatorCode = req.Invoice.OperatorCode;
            //IssuedDT = resp.Header.SendDateTime;

        }

        private XmlDocument CreateXMLElement(XmlDocument doc, XmlElement signatureInformation)
        {
            //XNamespace ext = "ext";
            //XNamespace sac = "urn:oasis:names:specification:ubl:schema:xsd:SignatureAggregateComponents-2";
            //XNamespace sig = "urn:oasis:names:specification:ubl:schema:xsd:CommonSignatureComponents-2";
            //XAttribute sacAttr = new XAttribute(XNamespace.Xmlns + "sac", sac);
            //XAttribute sigAttr = new XAttribute(XNamespace.Xmlns + "sig", sig);

            //XElement UBLExtension = new XElement(ext + "UBLExtension", sacAttr, sigAttr);

            var extensions = doc.GetElementsByTagName("Signature");
            extensions.Item(0).InnerXml = signatureInformation.InnerXml;

            doc.ImportNode(extensions.Item(0), true);
            //XElement request = new XElement("root",
            //     new XAttribute(XNamespace.Xmlns + "ext", ext),
            //        new XElement(ext + "UBLExtensions",
            //                new XElement(ext + "UBLExtension", sacAttr, sigAttr,
            //                    new XElement(ext + "ExtensionContent",
            //                        new XElement(sig + "UBLDocumentSignatures",
            //                            new XElement(sac + "SignatureInformation", signatureInformation))))
            //            ));

            return doc;

            //XmlDocument doc = new XmlDocument();
            //doc.LoadXml(request.ToString());

            //StringBuilder sb = new StringBuilder("");
            //var nodeList = doc.SelectNodes("root");
            //foreach (XmlNode node in nodeList)
            //{
            //    sb.Append(node.InnerXml);
            //}

            //XmlDocument doc2 = new XmlDocument();
            //doc2.LoadXml(sb.ToString());

            //return doc2;
        }

        public void Send(string webServiceAddress)
        {
            EinvoiceServicePortTypeClient FPC;

            if ((_webServiceAddress != "") && (_webServiceAddress != null))
            {
                BasicHttpBinding binding = new BasicHttpBinding(BasicHttpSecurityMode.Transport);
                EndpointAddress address = new EndpointAddress(_webServiceAddress);
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls12;
                FPC = new EinvoiceServicePortTypeClient(binding, address);
            }
            else
            {
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls12;
                FPC = new EinvoiceServicePortTypeClient();
            }

            EndpointBehavior eb = new EndpointBehavior(this);
            FPC.Endpoint.Behaviors.Add(eb);


            //string s = Serialize(this.req);
            //var doc = new XmlDocument();
            //var doc2 = new XmlDocument();
            //doc2.LoadXml(s);

            //XmlDocument xdoc = new XmlDocument();
            //xdoc = RemoveXmlns(s);
            //s=AddNamespace(xdoc.InnerXml, "RegisterEinvoiceRequest");
            //doc.LoadXml(s);
            //System.Security.Cryptography.Xml.SignedXml signedDoc = cert.GetSignedXmlDoc("#Request", doc);
            //XmlElement signature = signedDoc.GetXml();
            //doc2.DocumentElement.AppendChild(doc2.ImportNode(signature, true));
            //req = Deserialize<RegisterEinvoiceRequest>(doc2.InnerXml);



            string s2 = Serialize(this.req);
            resp = FPC.registerEinvoice(req);
        }

        public static XmlDocument RemoveXmlns(String xml)
        {
            XDocument d = XDocument.Parse(xml);
            d.Root.Descendants().Attributes().Where(x => x.IsNamespaceDeclaration).Remove();

            foreach (var elem in d.Descendants())
                elem.Name = elem.Name.LocalName;

            var xmlDocument = new XmlDocument();
            xmlDocument.Load(d.CreateReader());

            return xmlDocument;
        }

        public override string ToString()
        {
            return Serialize(this.req);
        }
    }
}
