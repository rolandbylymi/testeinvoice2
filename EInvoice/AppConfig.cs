﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Cache;
using System.Text;
using System.Threading.Tasks;

namespace EInvoice
{
    public static class AppConfig
    {
        private static string req;
        private static string xmlPath, reqInnerXML, globalIIC, globalOperatorCode, globalTCRCode, globalPayType, globalBUCode;
        public static string ServiceURI
        {
            get { return Config("ServiceURI"); }
        }

        public static string CN
        {
            get { return Config("CN"); }
        }

        public static string KeyStoreLocation
        {
            get { return Config("KeyStoreLocation"); }
        }

        public static string KeyStorePass
        {
            get { return Config("KeyStorePass"); }
        }

        public static string SoftCode
        {
            get { return Config("SoftCode"); }
        }

        public static string BusinUnitCode
        {
            get { return Config("BusinUnitCode"); }
        }

        public static string NUIS
        {
            get { return Config("NUIS"); }
        }

        public static string Req
        {
            get { return req; }   // get method
            set { req = value; }  // set method
        }

        public static string XMLPath
        {
            get { return Config("XMLPath"); } // get method
            set { xmlPath = value; }  // set method
        }

        public static string ReqInnerXML
        {
            get { return reqInnerXML; } // get method
            set { reqInnerXML = value; }  // set method
        }

        public static string GlobalIIC
        {
            get { return globalIIC; } // get method
            set { globalIIC = value; }  // set method
        }

        public static string GlobalOperatorCode
        {
            get { return globalOperatorCode; } // get method
            set { globalOperatorCode = value; }  // set method
        }

        public static string GlobalTCRCode
        {
            get { return globalTCRCode; } // get method
            set { globalTCRCode = value; }  // set method
        }

        public static string GlobalPayType
        {
            get { return globalPayType; } // get method
            set { globalPayType = value; }  // set method
        }

        public static string GlobalBUCode
        {
            get { return globalBUCode; } // get method
            set { globalBUCode = value; }  // set method
        }

        public static string TCRCode
        {
            get { return Config("TCRCode"); }
        }
        public static string OperatorCode
        {
            get { return Config("OperatorCode"); }
        }
        public static string UOM
        {
            get { return Config("UOM"); }
        }
        public static string IsCashUnit
        {
            get { return Config("IsCashUnit"); }
        }
        public static string TCRExtra
        {
            get { return Config("TCRExtra"); }
        }
        public static string InvoiceExtra
        {
            get { return Config("InvoiceExtra"); }
        }
        public static string WNTExtra
        {
            get { return Config("WNTExtra"); }
        }

        public static string MainteinerCode
        {
            get { return Config("MaintainerCode"); }
        }

        private static string Config(string arg)
        {
            return (ConfigurationManager.AppSettings[arg] == null) ?
                "" :
                ConfigurationManager.AppSettings[arg];
        }
    }
}
